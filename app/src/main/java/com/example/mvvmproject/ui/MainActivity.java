package com.example.mvvmproject.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.view.View;
import com.example.mvvmproject.R;
import com.example.mvvmproject.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity{

    /** MVVM -> (Model View ViewModel) Architecture Design Pattern *
    * Model -> Some Data From DB
    * View -> Layout XML And Activity (Control View UI Only)
    * View Model -> Class extends ViewModel (بيتحكم في البيانات وعرضاها عن طريق الاكتيفتي)
    **/

    /**
        * DataBinding
        * in gradle Module File in Android Section Set
        * dataBinding{enabled = true}
    **/

//    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /* Initialize Layout And Declaration Views By Default Definition UI
           * FindViewById And Declaration Layout By setContentView.
        */
//      setContentView(R.layout.activity_main);
//      TextView textView = findViewById(R.id.textView);
//      Button button = findViewById(R.id.button);

        /* Declaration And Initialize DataBinding
           * Can Invoked It By (Inverse Name Your Activity + Adding KeyWords (Binding)
        */
        final ActivityMainBinding binding = DataBindingUtil.setContentView(this,R.layout.activity_main);

        // Declaration And Initialize ViewModel
        final MovieViewModel movieViewModel = ViewModelProviders.of(this).get(MovieViewModel.class);
        movieViewModel.movieNameMutableLiveData.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String name) {
                binding.textView.setText(name);
            }
        });

        binding.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                movieViewModel.getMovieName();
            }
        });

    }

}
