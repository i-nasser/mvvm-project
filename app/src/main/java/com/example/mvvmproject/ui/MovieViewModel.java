package com.example.mvvmproject.ui;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.mvvmproject.Pojo.MovieModel;

class MovieViewModel extends ViewModel {

    MutableLiveData<String> movieNameMutableLiveData = new MutableLiveData<>();

    void getMovieName() {
        String movieName = getMovieNameFromDataBase().getName();
        movieNameMutableLiveData.setValue(movieName);
    }

    private MovieModel getMovieNameFromDataBase() {
        return new MovieModel("Nasser Movie","1");
    }

}
